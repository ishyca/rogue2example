﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {


	public static GameManager instance = null;
	public BroadManager boardScript;
	public int playerFoodPoints = 100;//starting points which preserved over levels
	[HideInInspector] public bool playersTurn = true;

	private int level = 3;


	void Awake () {
		if (instance == null) {
			instance = this;
		} else if(instance != this) {
			Destroy (this.gameObject);
		}
		DontDestroyOnLoad (gameObject);
		this.boardScript = GetComponent<BroadManager> ();
		this.InitGame();
	}

	void InitGame(){
		this.boardScript.SetupScene(this.level);
	}


	public void GameOver(){ 
		this.enabled = false;
	}

	// Update is called once per frame
	void Update (){
	
	}
}
